<?php

require_once '../../vendor/autoload.php';

use Controler\Controler;
use Illuminate\Database\Capsule\Manager as DB;

//Connexion 

	$tab = parse_ini_file('../conf/conf.ini');
	$db = new DB();
	$db->addConnection($tab);
	$db->setAsGlobal();
	$db->bootEloquent();
	
$app = new \Slim\Slim();

$app-> get('/', function(){
	$aff = new Controler();
	$content= $aff->index();
	echo $content;
});

$app-> get('/req1', function(){
	$aff = new Controleur();
	$content= $aff->req1();
	echo $content;
});

$app-> get('/req2', function(){
	$aff = new Controleur();
	$content= $aff->req2();
	echo $content;
});

$app -> run();
