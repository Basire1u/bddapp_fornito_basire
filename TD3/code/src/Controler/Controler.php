<?php

namespace Controler;

use ..\Model\Annonce;
use ..\Model\Categorie;
use ..\Model\Photo;
use Illuminate\Database\Capsule\Manager as DB;

class Controler{
	
	function index()
	{
		echo "Index";
	}
	
	function req1()
	{
		$start=microtime(true);
		$liste=Game::where('name', 'like', 'Mario%');
			->whereHas('original_game_ratings', function($q){
				$q->where('name', 'like', '%3+%');
			})
			->get();
		$time = microtime(true)-$start;
		echo "durée requête 4 : ". $time . "\n";
	}
	
	function req2()
	{
		DB::connection()->enableQuery();
		foreach( DB::getQueryLog() as $q)
		{
			echo "--------------------- \n";
			echo "query : " . $q['query'] . "\n";
			echo " --- bindings : [ ";
			
			foreach ($q['bindings'] as $b)
			{
				echo " ". $b . ",";
			}
			
			echo " ] ---\n";
			echo "------------------- \n \n";
		};
	}
}