<?php
require_once 'vendor/autoload.php';

include_once 'src\GamePedia\Controler\Controleur.php';

$db = new \Illuminate\Database\Capsule\Manager;
//Connexion 
	$param = parse_ini_file('src/conf/conf.ini');
	$db->addConnection([
	'driver'=>$param['driver'],
	'host'=>$param['host'],
	'database'=>$param['database'],
	'username'=>$param['user'],
	'password'=>$param['pass'],
	'charset'=>'utf8',
	'collation'=>'utf8_unicode_ci',
	'prefix'=>''
	]);
	$db->setAsGlobal();
	$db->bootEloquent();
	
$app = new \Slim\Slim;


$app->get('/', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->index();
	echo $content;
});


//Partie 1 (Modifié en Partie 6)

/*
$app->get('/api/games/:id', function($id){
	global $app; 
	$aff = new GamePedia\Controleur();
	$content = $aff->repJson($id);
	$app->response->headers->set('Content-Type', 'application/json');
	echo json_encode($content, JSON_PRETTY_PRINT);
});
*/
/*
//Partie 2(Modifié en Partie 3)

$app->get('/api/games', function(){
	global $app;
	$aff = new GamePedia\Controleur();
	$content = $aff->repJson200();
	$app->response->headers->set('Content-Type', 'application/json');
	echo json_encode($content, JSON_PRETTY_PRINT);
});
*/

//Partie 3 et 4

$app->get('/api/games', function(){
	global $app;
	$aff = new GamePedia\Controleur();
	$content = $aff->page();
	$app->response->headers->set('Content-Type', 'text/html');
	echo $content;
});

//Partie 5

$app->get('/api/games/:id/comments', function($id){
	global $app; 
	$aff = new GamePedia\Controleur();
	$content = $aff->commentaireJeu($id);
	$app->response->headers->set('Content-Type', 'application/json');
	echo json_encode($content, JSON_PRETTY_PRINT);
});

//Partie 6


$app->get('/api/games/:id/character', function($id){
	global $app; 
	$aff = new GamePedia\Controleur();
	$content = $aff->personnageJeu($id);
	$app->response->headers->set('Content-Type', 'application/json');
	echo json_encode($content, JSON_PRETTY_PRINT);
});

$app->get('/api/games/:id/platform', function($id){
	global $app; 
	$aff = new GamePedia\Controleur();
	$content = $aff->platformJeu($id);
	$app->response->headers->set('Content-Type', 'text/html');
	echo $content;
});


$app->get('/api/games/:id', function($id){
	global $app; 
	$aff = new GamePedia\Controleur();
	$content = $aff->idJeu($id);
	$app->response->headers->set('Content-Type', 'text/html');
	echo $content;
});

$app -> run();
