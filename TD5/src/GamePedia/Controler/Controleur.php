<?php

namespace GamePedia;

require_once 'vendor/autoload.php';

include_once 'src\GamePedia\models\company.php';
include_once 'src\GamePedia\models\game.php';
include_once 'src\GamePedia\models\game2platform.php';
include_once 'src\GamePedia\models\platform.php';
include_once 'src\GamePedia\models\utilisateur.php';
include_once 'src\GamePedia\models\commentaire.php';
include_once 'src\GamePedia\models\character.php';

class Controleur
{
	
	function index()
	{}
	
	//Partie 1
	
	function repJson($id)
	{
		return $req = game::select('id', 'name', 'alias', 'deck', 'original_release_date')->where('id','=',$id)->get();
	}
	
	//Partie 2
	function repJson200()
	{
		return $req = game::select('id', 'name', 'alias', 'deck', 'original_release_date')->where('id','<=', 200)->get();
	}
	
	//Partie 3 et 4
	
	function page()
	{
		$jeuParPage = 200;
		$nbJeu = game::select('*')->count();
		$nbPage = $nbJeu / $jeuParPage;
		$pageActuelle = 0;
		
		if(isset($_GET['page'])) 
		{
			 $pageActuelle=intval($_GET['page']);
			 
			 if($pageActuelle > $nbPage)
			 {
				$pageActuelle = $nbPage;
			 }
		}
		
		$nbToSkip = $pageActuelle * $jeuParPage;
		
		$listJeu = game::select('id', 'name', 'alias', 'deck')->skip($nbToSkip)->take($jeuParPage)->get();
		
		$content1="<ul>";
		
		foreach($listJeu as $jeu)
		{
			$content1.="<p>";	
			$content1.="<li>".$jeu->id."</li>";	
			$content1.="<li>".$jeu->name."</li>";	
			$content1.="<li>".$jeu->alias."</li>";	
			$content1.="<li>".$jeu->deck."</li>";
			$content1.='<li><a href="./games/'.$jeu->id.'">Lien details du jeu</a></li>';				
			$content1.="</p>";
		}
		
		$content1.="</ul>";
		
		$pageAvant = $pageActuelle - 1;
		$pageApres = $pageActuelle + 1;
		
		$content1 .= '<p align="center"><a href="./games?page='.$pageAvant.'">Page précédente</a>  |  <a href="./games?page='.$pageApres.'">Page suivante</a>';
		
		return $content1;	
	}
	
	//Partie 5
	
	function commentaireJeu($id)
	{
		return $req = commentaire::select('emailUtilisateur', 'created_at', 'titre', 'contenu')->where('idJeu','=',$id)->get();
	}
	
	//Partie 6
	
	function personnageJeu($id)
	{
		return $req = character::select('*')->where('first_appeared_in_game_id','=',$id)->get();
	}
	
	function platformJeu($id)
	{
		$content3 = "";
		$id_platform = game2platform::select('platform_id')->where('game_id', '=', $id)->get();
		foreach($id_platform as $ligne)
		{
			$id_ligne = $ligne->platform_id;
			$list = platform::select('id', 'name', 'alias', 'abbreviation')->where('id', '=', $id_ligne)->get();
			
			$content3 .="<ul>";
			foreach($list as $j)
			{	
				$content3.="<li>".$j->id."</li>";	
				$content3.="<li>".$j->name."</li>";	
				$content3.="<li>".$j->alias."</li>";	
				$content3.="<li>".$j->abbreviation."</li>";	
			}
			$content3 .= "</ul>";
		}
		
		return $content3;
	}
	
	function idJeu($id)
	{
		$listJeu = game::select('id', 'name', 'alias', 'deck', 'original_release_date')->where('id','=',$id)->get();
		
		$content2="<ul>";
		$content2.="<p> Game : </p>";
		foreach($listJeu as $jeu)
		{	
			$content2.="<li>".$jeu->id."</li>";	
			$content2.="<li>".$jeu->name."</li>";	
			$content2.="<li>".$jeu->alias."</li>";	
			$content2.="<li>".$jeu->deck."</li>";	
			$content2.="<li>".$jeu->original_release_date."</li>";			
		}
		$content2.="</ul>";
		$content2.="<ul>";
		$content2.="Links :";
		$content2.='<li><a href="./'.$id.'/comments">Lien commentaires du jeu</a></li>';	
		$content2.='<li><a href="./'.$id.'/character">Lien personnages du jeu</a></li>';
		$content2.='<li><a href="./'.$id.'/platform">Lien plateform du jeu</a></li>';
		$content2.="</ul>";
		
		return $content2;
	}
}