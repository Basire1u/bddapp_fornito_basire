<?php
namespace Models;
use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model
{
    protected $table = 'Utilisateur';
    protected $primaryKey = 'email';
    public $timestamps = false;
	
	
	public static function createUser($email, $nom, $prenom, $adresse, $tel, $dateNaiss) {
		$uti = new Utilisateur();
		$uti->email = $email;
		$uti->nom = $nom;
		$uti->prenom = $prenom;
		$uti->adresse = $adresse;
		$uti->tel = $tel;
		$uti->dateNaiss = $dateNaiss;
		$uti->save();
	}

}



