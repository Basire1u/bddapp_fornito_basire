<?php

namespace GamePedia;

class Game2Platform extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'game2platform';
    protected $primaryKey = 'game_id , platform_id';
    public $timestamps = false;

}