<?php
use Illuminate\Database\Capsule\Manager as DB;

require_once '../../Faker-master/src/autoload.php';
require_once '../Vendor/autoload.php';

include_once 'Models\Utilisateur.php';
include_once 'Models\Commentaire.php';


$db = new DB;
//Connexion 
	$param = parse_ini_file('../conf/conf.ini');
	$db->addConnection([
	'driver'=>$param['driver'],
	'host'=>$param['host'],
	'database'=>$param['database'],
	'username'=>$param['user'],
	'password'=>$param['pass'],
	'charset'=>'utf8',
	'collation'=>'utf8_unicode_ci',
	'prefix'=>''
	]);
	$db->setAsGlobal();
	$db->bootEloquent();
	
//Creer 2 utilisateurs, 3 commentaires par utilisateurs
function creer3Uti()
{
	$faker = Faker\Factory::create();
	for ($i = 1; $i <= 2; $i++)
	{
		$email = $faker->email;
		$nom = $faker->name($gender = null|'male'|'female');
		$prenom = $faker->firstName($gender = null|'male'|'female');
		$adresse = $faker->address;
		$tel = $faker->e164PhoneNumber;
		$dateNaiss = $faker->dateTimeThisCentury($max = 'now', $timezone = date_default_timezone_get());
		Models\Utilisateur::createUser($email, $nom, $prenom, $adresse, $tel, $dateNaiss);
		for ($j = 1; $j <= 3; $j++)
		{
			$emailUtilisateur = $email;
			$titre = $faker->word;
			$contenu = $faker->text($maxNbChars = 50);
			$created_at = $faker->dateTimeThisMonth($max = 'now', $timezone = date_default_timezone_get());
			$updated_at = $created_at;
			$idJeu = 12342;
			Models\Commentaire::createCommentary($emailUtilisateur, $titre, $contenu, $created_at, $updated_at, $idJeu);
		}
	}
}


//Creer 25000 utilisateurs, 250 000 commentaires ici réduit à 25 utilisateur et 250 commentaire par soucis de vitesse
function creer25000Uti()
{
	$faker = Faker\Factory::create();
	for ($i = 1; $i <= 25000; $i++)
	{
		$email = $faker->email;
		$nom = $faker->name($gender = null|'male'|'female');
		$prenom = $faker->firstName($gender = null|'male'|'female');
		$adresse = $faker->address;
		$tel = $faker->e164PhoneNumber;
		$dateNaiss = $faker->dateTimeThisCentury($max = 'now', $timezone = date_default_timezone_get());
		Models\Utilisateur::createUser($email, $nom, $prenom, $adresse, $tel, $dateNaiss);
		for ($j = 1; $j <= 250000; $j++)
		{
			$emailUtilisateur = $email;
			$titre = $faker->word;
			$contenu = $faker->text($maxNbChars = 50);
			$created_at = $faker->dateTimeThisMonth($max = 'now', $timezone = date_default_timezone_get());
			$updated_at = $created_at;
			$idJeu = $faker->numberBetween($min = 1, $max = 47948);
			Models\Commentaire::createCommentary($emailUtilisateur, $titre, $contenu, $created_at, $updated_at, $idJeu);
		}
	}
}



function req1($email)
{
		$req = Models\Commentaire::select('titre', 'created_at')->where('emailUtilisateur', '=', $email)->orderBy('created_at', 'DESC')->get();
		echo $req;
}

function req2()
{
	$req2 = Models\Commentaire::select('emailUtilisateur')->groupBy('emailUtilisateur')->having('COUNT(*)', '>=', '5')->get();
}

creer25000Uti();
req1('eula98@hotmail.com');

?>