<?php
namespace Models;
use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
    protected $table = 'commentaire';
    protected $primaryKey = 'emailUtilisateur , created_at';
    public $timestamps = true;
	
	public static function createCommentary($emailUtilisateur, $titre, $contenu, $created_at, $updated_at, $idJeu) {
		$comm = new Commentaire();
		$comm->emailUtilisateur = $emailUtilisateur;
		$comm->titre = $titre;
		$comm->contenu = $contenu;
		$comm->created_at = $created_at;
		$comm->updated_at = $updated_at;
		$comm->idJeu = $idJeu;
		$comm->save();
	}

}