<?php
require_once 'vendor/autoload.php';

include_once 'src\GamePedia\Controler\Controleur.php';

$db = new \Illuminate\Database\Capsule\Manager;
//Connexion 
	$param = parse_ini_file('src/conf/conf.ini');
	$db->addConnection([
	'driver'=>$param['driver'],
	'host'=>$param['host'],
	'database'=>$param['database'],
	'username'=>$param['user'],
	'password'=>$param['pass'],
	'charset'=>'utf8',
	'collation'=>'utf8_unicode_ci',
	'prefix'=>''
	]);
	$db->setAsGlobal();
	$db->bootEloquent();
	
$app = new \Slim\Slim;

$app->get('/', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->index();
	echo $content;
});

$app->get('/Mario', function(){
	$aff = new GamePedia\Controleur();
	$content= $aff->chercherMario();
	echo $content;
});

$app->get('/Japon', function(){
	$aff = new GamePedia\Controleur();
	$content= $aff->chercherJapon();
	echo $content;
});

$app->get('/Plateforme', function(){
	$aff = new GamePedia\Controleur();
	$content= $aff->chercherPlateforme();
	echo $content;
});

$app->get('/442', function(){
	$aff = new GamePedia\Controleur();
	$content= $aff->chercher442();
	echo $content;
});

$app->get('/ListeJeux', function(){
	$aff = new GamePedia\Controleur();
	$content= $aff->listeJeux();
	echo $content;
});

$app -> run();
