<?php

require_once '../vendor/autoload.php';

include_once '../code\src\Controleur\ControleurPreparation.php';

$db = new \Illuminate\Database\Capsule\Manager;
//Connexion 
	$param = parse_ini_file('../code/conf/conf.ini');
	$db->addConnection([
	'driver'=>$param['driver'],
	'host'=>$param['host'],
	'database'=>$param['database'],
	'username'=>$param['user'],
	'password'=>$param['pass'],
	'charset'=>'utf8',
	'collation'=>'utf8_unicode_ci',
	'prefix'=>''
	]);
	
	$db->setAsGlobal();
	$db->bootEloquent();
	
$app = new \Slim\Slim;

$app-> get('/', function(){
	$aff = new photo\Controleur();
	$content = $aff->index();
	echo $content;
});

$app-> get('/req1', function(){
	$aff = new photo\Controleur();
	$content = $aff->req1();
	echo $content;
});

$app-> get('/req2', function(){
	$aff = new photo\Controleur();
	$content = $aff->req2();
	echo $content;
});

$app-> get('/req3', function(){
	$aff = new photo\Controleur();
	$content = $aff->req3();
	echo $content;
});

$app-> get('/req4', function(){
	$aff = new photo\Controleur();
	$content = $aff->req4();
	echo $content;
});

$app-> get('/req5', function(){
	$fonc = new photo\Controleur();
	$fonc->req5();
	echo 'Ajout photo annonce 22 fait';
});

$app-> get('/req6', function(){
	$fonc = new photo\Controleur();
	$fonc->req6();
	echo 'Ajout annonce 22 au categorie 42 et 73';
});

$app -> run();