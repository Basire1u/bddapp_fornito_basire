--
--Structure de la table Annonce

--

CREATE TABLE Annonce (
   id int(11) NOT NULL,
   titre varchar(20) DEFAULT NULL,
   date_Annonce date DEFAULT NULL,   
   texte varchar(100) DEFAULT NULL,
   PRIMARY KEY (id)
);

CREATE TABLE Categorie (
   id int(11) NOT NULL,
   nom varchar(20) DEFAULT NULL,  
   descr varchar(20) DEFAULT NULL,
   PRIMARY KEY (id)
);

CREATE TABLE Photo (
   id int(11) NOT NULL,
   file_path varchar(50) DEFAULT NULL,
   date_Photo date DEFAULT NULL,    
   taille_octet bigint NOT NULL,
   id_annonce int(11) DEFAULT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_annonce) REFERENCES Annonce(id)
);

CREATE TABLE AssocAnnonceCategorie (
   id_annonce int(11) NOT NULL,
   id_categorie int(11) NOT NULL,
   FOREIGN KEY (id_annonce) REFERENCES Annonce(id),
   FOREIGN KEY (id_categorie) REFERENCES Categorie(id)
);



INSERT INTO `photo`(`id`, `file_path`, `date_Photo`, `taille_octet`, `id_annonce`) VALUES (1,"bddapp_fornito_basire/TD2/preparation/donne/1",null,150000,1);

INSERT INTO `photo`(`id`, `file_path`, `date_Photo`, `taille_octet`, `id_annonce`) VALUES (2,"bddapp_fornito_basire/TD2/preparation/donne/2",null,10000,1);

INSERT INTO `photo`(`id`, `file_path`, `date_Photo`, `taille_octet`, `id_annonce`) VALUES (3,"bddapp_fornito_basire/TD2/preparation/donne/3",null,15000,1);


INSERT INTO `photo`(`id`, `file_path`, `date_Photo`, `taille_octet`, `id_annonce`) VALUES (4,"bddapp_fornito_basire/TD2/preparation/donne/4",null,150000,22);

INSERT INTO `photo`(`id`, `file_path`, `date_Photo`, `taille_octet`, `id_annonce`) VALUES (5,"bddapp_fornito_basire/TD2/preparation/donne/5",null,1000,22);


INSERT INTO `photo`(`id`, `file_path`, `date_Photo`, `taille_octet`, `id_annonce`) VALUES (6,"bddapp_fornito_basire/TD2/preparation/donne/6",null,1000,3);

INSERT INTO `photo`(`id`, `file_path`, `date_Photo`, `taille_octet`, `id_annonce`) VALUES (7,"bddapp_fornito_basire/TD2/preparation/donne/7",null,1000,3);

INSERT INTO `photo`(`id`, `file_path`, `date_Photo`, `taille_octet`, `id_annonce`) VALUES (8,"bddapp_fornito_basire/TD2/preparation/donne/8",null,1500,3);


INSERT INTO `photo`(`id`, `file_path`, `date_Photo`, `taille_octet`, `id_annonce`) VALUES (9,"bddapp_fornito_basire/TD2/preparation/donne/9",null,150000,2);


INSERT INTO `annonce`(`id`, `titre`, `date_Annonce`, `texte`) VALUES (1,"cat 1",null,"ceci est le texte de la cat 1");
INSERT INTO `annonce`(`id`, `titre`, `date_Annonce`, `texte`) VALUES (2,"cat 2",null,"ceci est le texte de la cat 2");
INSERT INTO `annonce`(`id`, `titre`, `date_Annonce`, `texte`) VALUES (3,"cat 3",null,"ceci est le texte de la cat 3");
INSERT INTO `annonce`(`id`, `titre`, `date_Annonce`, `texte`) VALUES (22,"cat 22",null,"ceci est le texte de la cat 22");