<?php

namespace GamePedia;

class Rating_board extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'rating_board';
    protected $primaryKey = 'id';
    public $timestamps = false;

}