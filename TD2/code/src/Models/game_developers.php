<?php

namespace GamePedia;

class Game_developers extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'game_developers';
    protected $primaryKey = 'game_id , comp_id';
    public $timestamps = false;

}