<?php

namespace photo;

class Photo extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'Photo';
    protected $primaryKey = 'id';
    public $timestamps = false;
}