<?php

namespace photo;

class Annonce extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'Annonce';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
