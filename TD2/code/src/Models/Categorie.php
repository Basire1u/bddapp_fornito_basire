<?php

namespace photo;

class Categorie extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'Categorie';
    protected $primaryKey = 'id';
    public $timestamps = false;
}