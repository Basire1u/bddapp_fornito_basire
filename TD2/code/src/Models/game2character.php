<?php

namespace GamePedia;

class Game2character extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'game2character';
    protected $primaryKey = 'game_id , character_id';
    public $timestamps = false;

}