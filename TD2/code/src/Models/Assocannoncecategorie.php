<?php

namespace photo;

class Assocannoncecategorie extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'Assocannoncecategorie';
    protected $primaryKey = 'id_annonce , id_categorie';
    public $timestamps = false;
}