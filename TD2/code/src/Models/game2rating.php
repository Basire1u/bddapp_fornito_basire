<?php

namespace GamePedia;

class Game2rating extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'game2rating';
    protected $primaryKey = 'id';
    public $timestamps = false;

}