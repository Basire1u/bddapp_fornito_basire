<?php

namespace GamePedia;

require_once '../../vendor/autoload.php';

include_once 'Models\game.php';
include_once 'Models\game2character.php';
include_once 'Models\character.php';
include_once 'Models\company.php';
include_once 'Models\game_developers.php';
include_once 'Models\game2rating.php';
include_once 'Models\game_rating.php';
include_once 'Models\rating_board.php';
include_once 'Models\genre.php';
include_once 'Models\game2genre.php';


class Controleur{
	
	function index(){
		return "test";
	}
	
	static function req1(){
		$html = 'Personnage du jeu 12342 : </br>';
		$req = game2character::select('character_id')->where('game_id', '=', 12342)->get();
		foreach($req as $ligne){
			$idCharacter = $ligne->character_id;
			$req2 = character::select('name', 'deck')->where('id', '=', $idCharacter)->get();
			foreach($req2 as $ligneChar){
				$html .= "<p> $ligneChar->name : $ligneChar->deck </p>";
			}
		} 
		return $html;
	}
	
	static function req2(){
		$html = "Personnage dont le nom du jeu debute par 'Mario' : </br>";
		$req = game::select('id' , 'name')->where('name','like','Mario%')->get();
		foreach($req as $ligne){
			$idGame = $ligne->id;
			$req2 = game2character::select('character_id')->where('game_id', '=', $idGame)->get();
			$html .= "<p> $ligne->name : </p>";
			
			foreach($req2 as $ligneIdChar){
				$idCharacter = $ligneIdChar->character_id;
				$req3 = character::select('name', 'deck')->where('id', '=', $idCharacter)->get();
				
				foreach($req3 as $ligneChar){
					$html .= "<p> $ligneChar->name : $ligneChar->deck </p>";
				}
			}
			$html .= "</br>";
		}		
		return $html;
	}
	
	static function req3(){
		$html = 'Les jeux develloper par un compagnie dont le nom commence par Sony : <br></br>';
		$req = company::select('id', 'name')->where('name','like','%Sony%')->get();
		foreach($req as $ligne){
			$idComp = $ligne->id;
			$req2 = game_developers::select('game_id')->where('comp_id', '=', $idComp)->get();
			
			$html .= "<p> $ligne->name : </p>";
			
			foreach($req2 as $ligneIdGame){
				$idGame = $ligneIdGame->game_id;
				$req3 = game::select('name', 'deck')->where('id', '=', $idGame)->get();
				foreach($req3 as $ligneGame){
					$html .= "<p> $ligneGame->name : $ligneGame->deck </p>";
				}	
			}
			$html .= "</br>";
		}
		return $html;
	}
	
	static function req4(){
		$html = 'Rating board des jeux mario: </br>';
		
		$req = game::select('id' , 'name')->where('name','like','Mario%')->get();
		foreach($req as $ligne){
			$idGame = $ligne->id;
			
			$req2 = game2rating::select('rating_id')->where('game_id', '=', $idGame)->get();
			$html .= "<p> $ligne->name : </p>";
			
			foreach($req2 as $ligneIdRating){
				$idRating = $ligneIdRating->rating_id;
				$req3 = game_rating::select('name', 'rating_board_id')->where('id', '=', $idRating)->get();
				
				foreach($req3 as $ligneRating){
					$html .= "<p> $ligneRating->name : $ligneRating->rating_board_id </p>";
				}
			}
			$html .= "</br>";
		}		
		return $html;
	}
	
	
	static function req5(){
		$html = 'Jeux Mario ayant plus de 3 personnage </br>';
		
		$req = game::select('id' , 'name')->where('name','like','Mario%')->get();
		
		foreach($req as $ligne){
			$idGame = $ligne->id;
			$req2 = game2character::select('character_id')->where('game_id', '=', $idGame)->count();
			
			if($req2 >= 4){
				$html .= "<p> $ligne->name : </p>";
			}			
		}		
		return $html;
	}
	
	static function req6(){
		$html = 'Jeux Mario ayant un rating contenant 3+ :';
		$html .= '</br>';
		
		$req = game::select('id' , 'name')->where('name','like','Mario%')->get();
		foreach($req as $ligne){
			$idGame = $ligne->id;
			
			$req2 = game2rating::select('rating_id')->where('game_id', '=', $idGame)->get();

			
			foreach($req2 as $ligneIdRating){
				$idRating = $ligneIdRating->rating_id;
				$req3 = game_rating::select('name', 'rating_board_id')->where('id', '=', $idRating)->where('name','like','%3+%')->get();
				
				foreach($req3 as $ligneRating){
					$html .= "<p> $ligne->name : </p>";;
				}
			}
		}		
		return $html;
		}
		
	static function req7(){
		$html = 'Jeux Mario ayant un rating contenant 3+ et publier par une compagnie dont le nom contient "Inc." :';
		$html .= '</br>';
		
		$req = company::select('id', 'name')->where('name','like','%Inc.%')->get();
		foreach($req as $ligne){
			$idComp = $ligne->id;
			$req2 = game_developers::select('game_id')->where('comp_id', '=', $idComp)->get();
			
			foreach($req2 as $ligneIdGame){
				$idGame = $ligneIdGame->game_id;
				$req3 = game::select('id', 'name', 'deck')->where('id', '=', $idGame)->get();
				
				foreach($req3 as $ligneGame){
					$idGame2 = $ligneGame->id;
					$req4 = game2rating::select('rating_id')->where('game_id', '=', $idGame2)->get();
					
					foreach($req4 as $ligneIdRating){
						$idRating = $ligneIdRating->rating_id;
						$req5 = game_rating::select('name', 'rating_board_id')->where('id', '=', $idRating)->where('name','like','%3+%')->get();
						
						foreach($req5 as $ligneRating){
							$html .= "<p> $ligneGame->name </p>";;
						}
					}	
				}
			}
		}
		return $html;
	}
	
	static function req8(){
		$html = 'Jeux Mario ayant un rating contenant 3+ et publier par une compagnie dont le nom contient "Inc." ET ayant recu un avis du rating_board "CERO" :';
		$html .= '</br>';
		
		$req = company::select('id', 'name')->where('name','like','%Inc.%')->get();
		foreach($req as $ligne){
			$idComp = $ligne->id;
			$req2 = game_developers::select('game_id')->where('comp_id', '=', $idComp)->get();
			
			foreach($req2 as $ligneIdGame){
				$idGame = $ligneIdGame->game_id;
				$req3 = game::select('id', 'name', 'deck')->where('id', '=', $idGame)->get();
				
				foreach($req3 as $ligneGame){
					$idGame2 = $ligneGame->id;
					$req4 = game2rating::select('rating_id')->where('game_id', '=', $idGame2)->get();
					
					foreach($req4 as $ligneIdRating){
						$idRating = $ligneIdRating->rating_id;
						$req5 = game_rating::select('name', 'rating_board_id')->where('id', '=', $idRating)->where('name','like','%3+%')->get();
					
						foreach($req5 as $ligneGame_rating){
							$idGameRating = $ligneGame_rating->rating_board_id;
							$req6 = rating_board::select('name')->where('id', '=', $idGameRating)->where('name','like','%CERO%')->count();
							if ($req6 >= 1){
								$html .= "<p> $ligneGame->name </p>";;
							}
						}
					}
				}
			}
		}
		return $html;
	}
	
	static function req9(){
		$newGenre = new genre;
		$req = genre::select('id')->orderBy('id', 'DESC')->first();
		$newGenre->id = $req->id + 1; 
		$newGenre->name = "Survival";
		$newGenre->deck = "Trying to survive with 1 or more character on an lost island or in a zombie apocalypse as well";
		$newGenre->description = null;
		$newGenre->save();
		
		$newGame2genre = new game2genre;
		$newGame2genre->game_id = 12;
		$newGame2genre->genre_id = $req->id + 1;
		$newGame2genre->save();
		
		$newGame2genre = new game2genre;
		$newGame2genre->game_id = 56;
		$newGame2genre->genre_id = $req->id + 1;
		$newGame2genre->save();
		
		$newGame2genre = new game2genre;
		$newGame2genre->game_id = 12;
		$newGame2genre->genre_id = $req->id + 1;
		$newGame2genre->save();
		
		$newGame2genre = new game2genre;
		$newGame2genre->game_id = 345;
		$newGame2genre->genre_id = $req->id + 1;
		$newGame2genre->save();
	}
}
