<?php

namespace photo;

require_once '../vendor/autoload.php';

include_once '../code\src\Models\Annonce.php';
include_once '../code\src\Models\Categorie.php';
include_once '../code\src\Models\Photo.php';
include_once '../code\src\Models\Assocannoncecategorie.php';

class Controleur{
	
	function index(){
		return "test";
	}
	
	static function req1(){
		$html = 'Photo numero 22 : </br>';
		$req = photo::select('id', 'file_path')->where('id_annonce', '=', '22')->get();
		foreach($req as $ligne){
			$html .= "<p> $ligne->id , $ligne->file_path</p>";
		} 
		return $html;
	}
	
	static function req2(){
		$html = "Photo d'une taille superieur a 10 000 octet dans l'annonce 22 : </br>";
		$req = photo::select('id', 'file_path')->where('taille_octet','>=','100000')->where('id_annonce', '=', 22)->get();
		foreach($req as $ligne){
			$html .= "<p> $ligne->file_path</p>";
		} 
		return $html;
		}
	
	static function req3(){
		$html = 'Annonce de plus de 3 photo : <br></br>';
		$tab = annonce::select('*')->get();
		foreach($tab as $assoc){
			$number = $assoc->id;
			$count = photo::select('*')->where('id_annonce', '=', $number)->count();
			if ($count >= 3){
				$req = annonce::select('id', 'titre')->where('id','=', $number)->get();
				foreach($req as $ligne){
					$html .= "<p> $ligne->id , $ligne->titre </p>";
				} 
			}
		}
		return $html;
	}
	
	static function req4(){
		$html = 'Annonce possédant des photo de plus de 100 000 octets : </br>';
		$tabAnnonce = annonce::select('*')->get();
		foreach($tabAnnonce as $assoc){
			$number = $assoc->id;
			$count = photo::select('*')->where('id_annonce', '=', $number)->where('taille_octet', '>=', 100000)->count();
			if ($count >= 1){
				$req = annonce::select('id', 'titre')->where('id','=', $number)->get();
				foreach($req as $ligne){
					$html .= "<p> $ligne->id , $ligne->titre </p>";
				}
			}		
		}
		return $html;
	}
	
	
	static function req5(){
		$newPhoto = new photo;
		
		$req = photo::select('id')->orderBy('id', 'DESC')->first();
		$newPhoto->id = $req->id + 1; 
		$newPhoto->file_path = "bla/ajout/req5";
		$newPhoto->date_Photo = null;
		$newPhoto->taille_octet = "15000";
		$newPhoto->id_annonce = "22";
		
		$newPhoto->save();
	}
	
		static function req6(){
		$newElement = new assocannoncecategorie;
		$newElement->id_annonce = 22;
		$newElement->id_categorie = 42;
		$newElement->save();
		
		$newElement2 = new assocannoncecategorie;
		$newElement2->id_annonce = 22;
		$newElement2->id_categorie = 73;
		$newElement2->save();
	}
	
}
