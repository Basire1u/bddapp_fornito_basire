<?php

require_once '../../vendor/autoload.php';

include_once 'Controleur\Controleur.php';

$db = new \Illuminate\Database\Capsule\Manager;
//Connexion 
	$param = parse_ini_file('../conf/conf.ini');
	$db->addConnection([
	'driver'=>$param['driver'],
	'host'=>$param['host'],
	'database'=>$param['database'],
	'username'=>$param['user'],
	'password'=>$param['pass'],
	'charset'=>'utf8',
	'collation'=>'utf8_unicode_ci',
	'prefix'=>''
	]);
	
	$db->setAsGlobal();
	$db->bootEloquent();
	
$app = new \Slim\Slim;

$app-> get('/', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->index();
	echo $content;
});

$app-> get('/req1', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->req1();
	echo $content;
});

$app-> get('/req2', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->req2();
	echo $content;
});

$app-> get('/req3', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->req3();
	echo $content;
});

$app-> get('/req4', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->req4();
	echo $content;
});

$app-> get('/req5', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->req5();
	echo $content;
});

$app-> get('/req6', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->req6();
	echo $content;
});

$app-> get('/req7', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->req7();
	echo $content;
});

$app-> get('/req8', function(){
	$aff = new GamePedia\Controleur();
	$content = $aff->req8();
	echo $content;
});

$app->get('/req9', function(){
	$fonc = new GamePedia\Controleur();
	$fonc->req9();
	echo 'Ajout du genre survival et associer aux jeux 12,56 et 345 et de nouveau 12';
});

$app -> run();